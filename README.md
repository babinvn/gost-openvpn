# OpenVPN GOST

Кому интересна ГОСТ-овая криптография... поэтому на русском.

Дистрибутив ALT Linux включает поддержку ГОСТ-овой криптографии. Чтобы `openvpn`
использовал отечественный алгоритм, необходимо включить следующие параметры в
файл конфигурации:

```
engine gost
cipher gost89-cbc
```

Здесь простой пример конфигурации point-to-point для OpenVPN с поддержкой GOST89

Собрать образ:
```
docker build -t openvpn-gost .
```
Сгенерировать ключ:
```
docker run --rm -it \
-v $(pwd):/etc/openvpn/ \
-w /etc/openvpn/ \
openvpn-gost \
openvpn --genkey secret secret.key
```
Запустить контейнеры:
```
docker-compose up -d
```
Дальше можно с клиента достучаться до сервера:
```
docker exec -it ovpn_gost_client \
ping -c 3 10.10.1.1
```
