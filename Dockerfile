FROM alt

RUN apt-get -y update \
  && apt-get -y dist-upgrade \
  && apt-get -y install openssl openssl-engines openssl-gost-engine openvpn \
  && apt-get clean

ENV OPENVPN=/etc/openvpn
COPY ovpn_run /usr/local/bin/
RUN chmod a+x /usr/local/bin/ovpn_run

CMD ["ovpn_run"]
